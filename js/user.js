const usuarioDAO = new PrincipalDAO();
let USUARIOS = 'obj_usuarios';

// Para Insertar un nuevo usuario.
let objUser1 = { id: 1, objeto: { user_id:'1' ,nombre: 'Julano', apellido: 'TAl', edad: '12' } };
usuarioDAO.insert(USUARIOS,objUser1);

//Para actualizar
objUser1 = { id: 1, objeto: { nombre: 'Otro', apellido: 'NOmbre', edad: '24' } };
usuarioDAO.update(USUARIOS,objUser1);

//Obtener registros
let getALl = usuarioDAO.getAll(USUARIOS);

//Obtener un registro por id
let byId = usuarioDAO.getById(USUARIOS,1);

//Eliminar por id
usuarioDAO.delete(USUARIOS,1);

//Eliminar todos los registros XD.
usuarioDAO.deleteAll(USUARIOS);
